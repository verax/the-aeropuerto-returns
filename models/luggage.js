var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var luggageSchema = new Schema({
        ticketID : //PK,
        weight : Number,
        insured : Boolean,
        size : String
});

var Luggage = mongoose.model('Luggage',luggageSchema);

module.exports = Luggage;
