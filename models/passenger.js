var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var passengerSchema= new Schema ({
        passportID : String,
        firstName : String,
        lastName : String,
        birthdate : Date
});

var Passenger = mongoose.model('Passenger',passengerSchema);

module.exports = Passenger;
