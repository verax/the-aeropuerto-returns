var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var airplaneSchema = new Schema ({
        airlineID : //Foreigner keys,
        model : String,
        luggageCapacity: Number,
        passengerCapacity: Number,
        seatColumns: Number,
        maximumDistance: Number,
        isOnMaintenance: Boolean,
        isOnTransit: Boolean
});

var Airplane = mongoose.model('Airplane', airplaneSchema);

module.exports = Airplane;
