var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var airlineSchema = new Schema ({
        name : String

});

var Airline = mongoose.model('Airline', airlineSchema);

module.exports = Airline;
