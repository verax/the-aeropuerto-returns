var mongoose = require('mongoose');

var Schema = mongoose.Schema

var airportSchema = new Schema({
        name : String,
        city : String,
        country : String,
        airplaneCapacity : Number
});

var Airport = mongoose.model('Airport',airportSchema);

module.exports = Airport;
