
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , airline = require('./routes/airline')
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose');

//mongoose.connect('mongodb://localhost/test');

var app = express();

app.locals.pretty = true;

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// Routes
app.get('/', routes.index);
app.get('/airlines', airline.index);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
