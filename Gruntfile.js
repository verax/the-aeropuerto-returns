module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-bower-install');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        bowerInstall: {
            target: {
                src: 'views/layout.jade',
                ignorePath: 'public/'
            }
        },
        watch: {
            js: {
                files: ['public/javascripts/*.js'],
                options: {
                    livereload: true
                }
            },
            css: {
                files: ['public/stylesheets/*.css'],
                options: {
                    livereload: true
                }
            },
            jade: {
                files: ['views/*.jade', 'routes/*.js'],
                options: {
                    livereload: true
                }
            }
        }
    });

    grunt.registerTask('default', ['watch']);
};
